object Form1: TForm1
  Left = 319
  Top = 182
  Width = 291
  Height = 208
  Caption = 'Cadastro de Pok'#233'mons'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Edit1: TEdit
    Left = 8
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object Button1: TButton
    Left = 8
    Top = 40
    Width = 121
    Height = 25
    Caption = 'inserir'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 8
    Top = 72
    Width = 121
    Height = 25
    Cursor = crArrow
    Caption = 'excluir'
    TabOrder = 2
    OnClick = Button2Click
  end
  object ListBox1: TListBox
    Left = 144
    Top = 8
    Width = 121
    Height = 121
    ItemHeight = 13
    Items.Strings = (
      '')
    TabOrder = 3
    OnClick = ListBox1Click
  end
  object Button3: TButton
    Left = 8
    Top = 104
    Width = 121
    Height = 25
    Caption = 'limpar tudo'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 8
    Top = 136
    Width = 121
    Height = 25
    Caption = 'Editar'
    TabOrder = 5
    Visible = False
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 144
    Top = 136
    Width = 121
    Height = 25
    Caption = 'Salvar Arquivo'
    TabOrder = 6
    OnClick = Button5Click
  end
end
