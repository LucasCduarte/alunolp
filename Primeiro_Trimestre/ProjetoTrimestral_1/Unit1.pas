unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    ed_Nome: TEdit;
    ed_Genero: TEdit;
    ed_Plataforma: TEdit;
    lb_nome: TLabel;
    lb_genero: TLabel;
    lb_plataforma: TLabel;
    bt_Enviar: TButton;
    Lista: TListBox;
    bt_Remover: TButton;
    bt_Atualizar: TButton;
    lb_Titulo: TLabel;
    bt_Salvar: TButton;
    ed_Nomearquivo: TEdit;
    lb_arquivo: TLabel;
    bt_Abrir: TButton;
    lb_ano: TLabel;
    ed_ano: TEdit;
    procedure bt_EnviarClick(Sender: TObject);
    procedure bt_RemoverClick(Sender: TObject);
    procedure bt_AtualizarClick(Sender: TObject);
    procedure ListaClick(Sender: TObject);
    procedure bt_SalvarClick(Sender: TObject);
    procedure bt_AbrirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1 : TForm1;

implementation
type
TJogo = class(TObject)
    Nome,Genero,Plataforma,ano : string;
  end;
var  Jogo : TJogo;
    Arquivo : TextFile;


{$R *.dfm}

procedure TForm1.bt_EnviarClick(Sender: TObject);
begin
Jogo:=TJogo.Create;
Jogo.Nome:=ed_Nome.Text;
Jogo.Genero:=ed_Genero.Text;
Jogo.Plataforma:=ed_Plataforma.Text;
Jogo.ano:=ed_ano.Text;
if (Jogo.Nome <> '') and (Jogo.Genero <> '') and (Jogo.Plataforma <> '') and (Jogo.ano <> '') then
begin
Lista.Items.Add('Nome: '+Jogo.Nome);
Lista.Items.Add('G�nero: '+Jogo.Genero);
Lista.Items.Add('Plataforma: '+Jogo.Plataforma);
Lista.Items.Add('Ano de Lan�amento: '+Jogo.ano);
lb_nome.visible:=false;
ed_Nome.visible:=false;
lb_genero.visible:=false;
ed_Genero.visible:=false;
lb_plataforma.visible:=false;
ed_Plataforma.visible:=false;
lb_ano.visible:=false;
ed_ano.visible:=false;
bt_Enviar.Visible:=false;
bt_Atualizar.Visible:=true;

ed_Nome.text:='';
ed_Genero.text:= '';
ed_Plataforma.text:='';
ed_ano.Text:='';

end else
begin
ShowMessage('Campo vazio');
end;
end;

procedure TForm1.bt_RemoverClick(Sender: TObject);
begin
if (Lista.ItemIndex <> 0) and (Lista.ItemIndex <> 1) and (Lista.ItemIndex <> 2) and (Lista.ItemIndex <> 3) then
begin
ShowMessage('Selecione um item!');
end else begin
if Lista.ItemIndex = 0 then
begin
Lista.DeleteSelected;
Lista.Items.Insert(0,'Nome: ');
end;
if Lista.ItemIndex = 1 then
begin
Lista.DeleteSelected;
Lista.Items.Insert(1,'G�nero: ');
end;
if Lista.ItemIndex = 2 then
begin
Lista.DeleteSelected;
Lista.Items.Insert(2,'Plataforma: ');
end;
if Lista.ItemIndex = 3 then
begin
Lista.DeleteSelected;
Lista.Items.Insert(3,'Ano de Lan�amento: ');
end;
end;
end;

procedure TForm1.bt_AtualizarClick(Sender: TObject);
begin
 Jogo:=TJogo.Create;
Jogo.Nome:=ed_Nome.Text;
Jogo.Genero:=ed_Genero.Text;
Jogo.Plataforma:=ed_Plataforma.Text;
Jogo.ano:=ed_ano.Text;
if (Lista.ItemIndex <> 0) and (Lista.ItemIndex <> 1) and (Lista.ItemIndex <> 2) and (Lista.ItemIndex <> 3) then
begin
ShowMessage('Selecione um item!');
end else begin
if Lista.ItemIndex = 0 then
begin
if (Ed_nome.text <> '') then begin
Form1.Lista.Items[Lista.ItemIndex]:= 'Nome: '+Jogo.Nome;
end else ShowMessage('Campo Vazio');
end;
if Lista.ItemIndex = 1  then
begin
if (Ed_Genero.text <> '') then begin
Form1.Lista.Items[Lista.ItemIndex]:= 'G�nero: '+Jogo.Genero;
end else ShowMessage('Campo Vazio');
end;
if Lista.ItemIndex = 2 then
begin
if (Ed_Plataforma.text <> '') then begin

Form1.Lista.Items[Lista.ItemIndex]:= 'Plataforma: '+Jogo.Plataforma;
end else ShowMessage('Campo Vazio');
end;
if Lista.ItemIndex = 3 then
begin
if (Ed_ano.text <> '') then begin

Form1.Lista.Items[Lista.ItemIndex]:= 'Ano de Lan�amento: '+Jogo.ano;
end else ShowMessage('Campo Vazio');
end;
end;
end;

procedure TForm1.ListaClick(Sender: TObject);
begin
if Lista.ItemIndex = 0 then
begin
lb_nome.visible:=true;
ed_Nome.visible:=true;
lb_genero.visible:=false;
ed_Genero.visible:=false;
lb_plataforma.visible:=false;
ed_Plataforma.visible:=false;
ed_ano.visible:=false;
lb_ano.visible:=false;
bt_Enviar.Visible:=false;
bt_Atualizar.Visible:=true;
end;
if Lista.ItemIndex = 1 then
begin
lb_nome.visible:=false;
ed_Nome.visible:=false;
lb_genero.visible:=true;
ed_Genero.visible:=true;
lb_plataforma.visible:=false;
ed_Plataforma.visible:=false;
ed_ano.visible:=false;
lb_ano.visible:=false;
bt_Enviar.Visible:=false;
bt_Atualizar.Visible:=true;
end;
if Lista.ItemIndex = 2 then
begin
lb_nome.visible:=false;
ed_Nome.visible:=false;
lb_genero.visible:=false;
ed_Genero.visible:=false;
lb_plataforma.visible:=true;
ed_Plataforma.visible:=true;
ed_ano.visible:=false;
lb_ano.visible:=false;
bt_Enviar.Visible:=false;
bt_Atualizar.Visible:=true;
end;
if Lista.ItemIndex = 3 then
begin
lb_nome.visible:=false;
ed_Nome.visible:=false;
lb_genero.visible:=false;
ed_Genero.visible:=false;
lb_plataforma.visible:=false;
ed_Plataforma.visible:=false;
ed_ano.visible:=true;
lb_ano.visible:=true;
bt_Enviar.Visible:=false;
bt_Atualizar.Visible:=true;
end;
end;

procedure TForm1.bt_SalvarClick(Sender: TObject);
begin
Jogo:=TJogo.Create;
Jogo.Nome:=Form1.Lista.Items[0];
Jogo.Genero:=Form1.Lista.Items[1];
Jogo.Plataforma:=Form1.Lista.Items[2];
Jogo.ano:=Form1.Lista.Items[3];
AssignFile(arquivo, ed_Nomearquivo.text+'.txt' );
Rewrite(arquivo);
WriteLn(arquivo,Jogo.Nome) ;
WriteLn(arquivo,Jogo.Genero);
WriteLn(arquivo,Jogo.Plataforma);
WriteLn(arquivo,Jogo.ano);

CloseFile(arquivo);
ShowMessage('Arquivo Salvo');
end;

procedure TForm1.bt_AbrirClick(Sender: TObject);
var linha: string;
begin
if ed_Nomearquivo.text = '' then
begin
Showmessage('Digite o nome do arquivo para localizar!')
end else begin
  Lista.Clear;
  AssignFile(arquivo, ed_Nomearquivo.text+'.txt');
 
  {$I-}
  Reset(arquivo);
  {$I+}

  if (IOResult <> 0)
     then Lista.items.Add('Arquivo N�o Encontrado!')
  else begin
         while (not eof(arquivo)) do
         begin
           readln(arquivo, linha);

           Lista.items.Add(linha);
         end;
 
         CloseFile(arquivo);
       end;
lb_nome.visible:=false;
ed_Nome.visible:=false;
lb_genero.visible:=false;
ed_Genero.visible:=false;
lb_plataforma.visible:=false;
ed_Plataforma.visible:=false;
lb_ano.visible:=false;
ed_ano.visible:=false;
bt_enviar.visible:=false;
bt_atualizar.visible:=true;


ed_Nome.text:='';
ed_Genero.text:= '';
ed_Plataforma.text:='';
ed_ano.Text:='';

       end;
        end;
end.
