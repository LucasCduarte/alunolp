object Form1: TForm1
  Left = 192
  Top = 125
  Width = 935
  Height = 277
  Caption = 'Jogos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  PixelsPerInch = 96
  TextHeight = 13
  object lb_nome: TLabel
    Left = 24
    Top = 56
    Width = 58
    Height = 25
    Caption = 'Nome:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lb_genero: TLabel
    Left = 16
    Top = 88
    Width = 71
    Height = 25
    Caption = 'G'#234'nero:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lb_plataforma: TLabel
    Left = 8
    Top = 120
    Width = 99
    Height = 25
    Caption = 'Plataforma:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lb_Titulo: TLabel
    Left = 8
    Top = 8
    Width = 181
    Height = 29
    Caption = 'Dados do Game:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lb_arquivo: TLabel
    Left = 584
    Top = 56
    Width = 154
    Height = 24
    Caption = 'Nome do Arquivo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lb_ano: TLabel
    Left = 8
    Top = 152
    Width = 176
    Height = 24
    Caption = 'Ano de Lan'#231'amento:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object ed_Nome: TEdit
    Left = 112
    Top = 56
    Width = 177
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object ed_Genero: TEdit
    Left = 112
    Top = 88
    Width = 177
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object ed_Plataforma: TEdit
    Left = 112
    Top = 120
    Width = 177
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object bt_Enviar: TButton
    Left = 24
    Top = 192
    Width = 89
    Height = 33
    Caption = 'Enviar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = bt_EnviarClick
  end
  object Lista: TListBox
    Left = 304
    Top = 56
    Width = 265
    Height = 89
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 20
    ParentFont = False
    TabOrder = 4
    OnClick = ListaClick
  end
  object bt_Remover: TButton
    Left = 136
    Top = 192
    Width = 89
    Height = 33
    Caption = 'Remover'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = bt_RemoverClick
  end
  object bt_Atualizar: TButton
    Left = 24
    Top = 192
    Width = 89
    Height = 33
    Caption = 'Atualizar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Visible = False
    OnClick = bt_AtualizarClick
  end
  object bt_Salvar: TButton
    Left = 624
    Top = 112
    Width = 89
    Height = 33
    Caption = 'Salvar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnClick = bt_SalvarClick
  end
  object ed_Nomearquivo: TEdit
    Left = 744
    Top = 56
    Width = 161
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
  end
  object bt_Abrir: TButton
    Left = 728
    Top = 112
    Width = 137
    Height = 33
    Caption = 'Abrir Arquivo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    OnClick = bt_AbrirClick
  end
  object ed_ano: TEdit
    Left = 192
    Top = 152
    Width = 97
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
  end
end
