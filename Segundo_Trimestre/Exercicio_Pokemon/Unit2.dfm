object DataModule2: TDataModule2
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 372
  Width = 624
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 'C:\EasyPHP-DevServer-14.1VC9\binaries\mysql\lib\libmysql.dll'
    Left = 136
    Top = 128
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=cedup'
      'User_Name=root'
      'DriverID=MySQL')
    Connected = True
    Left = 256
    Top = 128
  end
  object FDQueryPokemon: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'Select * from pokemon')
    Left = 352
    Top = 128
  end
  object DataSourcePokemon: TDataSource
    DataSet = FDQueryPokemon
    Left = 456
    Top = 128
  end
  object FDQueryTreinador: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'Select * from treinador')
    Left = 352
    Top = 200
  end
  object DataSourceTreinador: TDataSource
    DataSet = FDQueryTreinador
    Left = 448
    Top = 200
  end
end
