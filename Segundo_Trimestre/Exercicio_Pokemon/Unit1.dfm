object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 342
  ClientWidth = 653
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBLookupListBox1: TDBLookupListBox
    Left = 8
    Top = 8
    Width = 201
    Height = 329
    Color = clWhite
    KeyField = 'id'
    ListField = 'nome'
    ListSource = DataModule2.DataSourcePokemon
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 230
    Top = 8
    Width = 415
    Height = 169
    Color = clWhite
    ParentBackground = False
    TabOrder = 1
    object DBText1: TDBText
      Left = 122
      Top = 31
      Width = 65
      Height = 17
      DataField = 'id'
      DataSource = DataModule2.DataSourcePokemon
    end
    object DBText2: TDBText
      Left = 122
      Top = 62
      Width = 65
      Height = 17
      DataField = 'id_treinador'
      DataSource = DataModule2.DataSourcePokemon
    end
    object DBText3: TDBText
      Left = 122
      Top = 134
      Width = 65
      Height = 17
      DataField = 'nivel'
      DataSource = DataModule2.DataSourcePokemon
    end
    object DBText4: TDBText
      Left = 122
      Top = 96
      Width = 65
      Height = 17
      DataField = 'nome'
      DataSource = DataModule2.DataSourcePokemon
    end
    object Label1: TLabel
      Left = 22
      Top = 31
      Width = 61
      Height = 13
      Caption = 'Identificador'
    end
    object Label2: TLabel
      Left = 22
      Top = 62
      Width = 46
      Height = 13
      Caption = 'Treinador'
    end
    object Label3: TLabel
      Left = 22
      Top = 96
      Width = 27
      Height = 13
      Caption = 'Nome'
    end
    object Label4: TLabel
      Left = 22
      Top = 134
      Width = 23
      Height = 13
      Caption = 'N'#237'vel'
    end
  end
  object bt_Editar: TButton
    Left = 352
    Top = 200
    Width = 161
    Height = 25
    Caption = 'Editar'
    TabOrder = 2
    OnClick = bt_EditarClick
  end
  object bt_Inserir: TButton
    Left = 352
    Top = 248
    Width = 161
    Height = 25
    Caption = 'Inserir'
    TabOrder = 3
    OnClick = bt_InserirClick
  end
  object bt_Excluir: TButton
    Left = 352
    Top = 299
    Width = 161
    Height = 25
    Caption = 'Excluir'
    TabOrder = 4
    OnClick = bt_ExcluirClick
  end
end
