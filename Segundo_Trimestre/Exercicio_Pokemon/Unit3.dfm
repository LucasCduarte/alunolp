object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 138
  ClientWidth = 229
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 16
    Width = 31
    Height = 13
    Caption = 'Nome:'
  end
  object Label2: TLabel
    Left = 32
    Top = 48
    Width = 46
    Height = 13
    Caption = 'Treinador'
  end
  object Label3: TLabel
    Left = 32
    Top = 75
    Width = 23
    Height = 13
    Caption = 'N'#237'vel'
  end
  object DBEdit1: TDBEdit
    Left = 96
    Top = 13
    Width = 121
    Height = 21
    DataField = 'nome'
    DataSource = DataModule2.DataSourcePokemon
    TabOrder = 0
  end
  object DBEdit3: TDBEdit
    Left = 96
    Top = 67
    Width = 121
    Height = 21
    DataField = 'nivel'
    DataSource = DataModule2.DataSourcePokemon
    TabOrder = 1
  end
  object Button1: TButton
    Left = 16
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 129
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 3
    OnClick = Button2Click
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 100
    Top = 40
    Width = 121
    Height = 21
    DataField = 'id_treinador'
    DataSource = DataModule2.DataSourcePokemon
    KeyField = 'id'
    ListField = 'nome'
    ListSource = DataModule2.DataSourceTreinador
    TabOrder = 4
  end
end
