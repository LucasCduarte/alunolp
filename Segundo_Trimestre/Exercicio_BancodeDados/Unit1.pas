unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Unit2, Data.DB, Vcl.StdCtrls, Vcl.Grids,
  Vcl.DBGrids;

type
  TForm1 = class(TForm)
    DBGrid1: TDBGrid;
    Abrir: TButton;
    btDelete: TButton;
    btinserir: TButton;
    procedure AbrirClick(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
    procedure btinserirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.AbrirClick(Sender: TObject);
begin
  DataModule2.FDQuery1.Open();
end;

procedure TForm1.btDeleteClick(Sender: TObject);
begin
  DataModule2.FDQuery1.Delete();
end;

procedure TForm1.btinserirClick(Sender: TObject);
begin
    DataModule2.FDQuery1.Append();
end;

end.
