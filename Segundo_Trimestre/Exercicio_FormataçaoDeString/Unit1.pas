unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Length: TButton;
    contains: TButton;
    trim: TButton;
    LowerCase: TButton;
    UpperCase: TButton;
    Replace: TButton;
    Memo1: TMemo;
    Memo2: TMemo;
    procedure LengthClick(Sender: TObject);
    procedure LowerCaseClick(Sender: TObject);
    procedure UpperCaseClick(Sender: TObject);
    procedure ReplaceClick(Sender: TObject);
    procedure containsClick(Sender: TObject);
    procedure trimClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  var conteudo : string;

implementation

{$R *.dfm}


procedure TForm1.containsClick(Sender: TObject);
var conteudo: string;
begin
conteudo := memo1.text;
memo2.Text := conteudo;
if  conteudo.Contains('ola') then
  begin
  Memo2.text:= ('tem "ola"');
  end
  else
  begin
    Memo2.text:=('nao tem "ola"');
end;

end;

procedure TForm1.LengthClick(Sender: TObject);
begin
  conteudo := Memo1.Text;
  Memo2.Text := conteudo.Length.ToString;
end;

procedure TForm1.LowerCaseClick(Sender: TObject);
begin
 conteudo := Memo1.Text;
 Memo2.text := conteudo.LowerCase(conteudo);

end;

procedure TForm1.ReplaceClick(Sender: TObject);
var troca: string;
begin
conteudo:= Memo1.Text;
Memo2.Text:= conteudo.Replace('s', 't');

end;

procedure TForm1.trimClick(Sender: TObject);
begin
  conteudo := Memo1.Text;
 Memo2.text := conteudo.Trim();
end;

procedure TForm1.UpperCaseClick(Sender: TObject);
begin
 conteudo := Memo1.Text;
 Memo2.text := conteudo.UpperCase(conteudo);
end;

end.
